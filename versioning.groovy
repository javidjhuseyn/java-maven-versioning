def incrementVersion(){
    echo 'Incrementing the version ...'
    sh 'mvn build-helper:parse-version versions:set \
    -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
    versions:commit'
    // match everything in between version tags and select the first one 
    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
    //IMAGE_NAME = matcher[0][1]
    def version = matcher[0][1]
    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
}

def buildApp(){
    echo 'Building the maven application ...'
    sh 'mvn clean package'
}

def buildDockerImage(){
    echo 'Building the docker image ...'
    withCredentials([usernamePassword(credentialsId: 'dockerhub-credentials', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]){
        sh "docker build -t javidjhuseyn/maven-test-app:${IMAGE_NAME} ."
        sh "echo $PASSWORD | docker login -u $USERNAME --pasword-stdin"
        sh "docker push javidjhuseyn/maven-test-app:${IMAGE_NAME}"
    }
}

def deployImage(){
    echo "Deploying the image: javidjhuseyn/maven-test-app:$IMAGE_NAME ..."
}

def versionUpdate(){
    echo 'Commiting version changes to the Git repository ...'
    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]){
        sh 'git config --global user.email "jenkins@jenkins.com"'
        sh 'git config --global user.name "jenkins-admin"'
        
        sh 'git status'
        sh 'git branch'
        sh 'git config --list'
        
        sh "git remote set-url origin https://${USERNAME}:XRMzuwsftAuqmqkzuonR@gitlab.com/javidjhuseyn/java-maven-versioning.git"
        // XRMzuwsftAuqmqkzuonR
        
        sh 'git add .'
        sh 'git commit -m "Update pom.xml" . '
        sh 'git push origin HEAD:main'
    }
}

return this
